# NativeScript <3 WatchOS

## What?

**WatchOS** is the mobile operating system of the Apple Watch. It is based on the iOS operating system and has many similar features. It's also very different in very specific and seemingly random ways.

NativeScript is an open-source framework to develop apps on the Apple **iOS** and Android platforms. **Currently, it doesn't support WatchOS.** It's also full of bugs and it's probably a terrible choice for real work, but whatever, I just work here.

We want to be able to **embed a WatchOS application in an existing NativeScript application** and have them **communicate** with eachother.

## Why?

Because dozens of people use the iWatch and they're apparently a viable target market, I don't know, I just work here.

## How?

### Start with a regular NativeScript application

I don't know your life, so we'll create a new app:

```
tns create my-app --template tns-template-blank-ng
npm install
```

Make it do something:

`app/home/home.component.html`
```html
<ActionBar class="action-bar">
    <Label class="action-bar-title" text="Home"></Label>
</ActionBar>

<StackLayout class="page">
        <Label text="{{ message }}" textWrap="true"></Label>
        <Button text="Do the thing" (tap)="onTap($event)" class="btn btn-primary btn-active"></Button>
</StackLayout>
```

`app/home/home.component.ts`
```ts
import { Component, OnInit, NgZone } from "@angular/core";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {
    counter = 0;
    message = "You haven't done the thing yet.";

    constructor() {
    }

    ngOnInit(): void {
    }

    public onTap(): void {
        ++this.counter;
        this.message = `You have done the thing ${ this.counter } times.`;
    }
}
```

Make sure it works:

```
tns run ios
```

![alt text](images/1.png "It works")

### Create a WatchOS application

Open XCode and create a new project.

![alt text](images/2.png "ios App with WatchKit App")

The name of the project that NativeScript generates out of "my-app" is "myapp":

![alt text](images/3.png "The generated ios app")

It's also assigned to the organization "Telerik", with the identifier "org.nativescript".

So call it "myapp", and assign it to the organization "Telerik", with the identifier "org.nativescript":

![alt text](images/4.png "Product Name: myapp; Organization Name: Telerik; Organization Identifier: org.nativescript")

Or do whatever you want*, I don't care.

Add a label and a button:

![alt text](images/5.png "It's just labels and buttons all the way down")

Drag and drop the outlets and make them do something:

`myapp/myapp WatchKit Extension/InterfaceController.m`
```objc
#import "InterfaceController.h"

@interface InterfaceController ()
@property (unsafe_unretained, nonatomic) IBOutlet WKInterfaceLabel *MessageLabel;
@property (unsafe_unretained, nonatomic) IBOutlet WKInterfaceButton *ThingButton;
@property int Counter;
@end

@implementation InterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    // Configure interface objects here.
    [_MessageLabel setText:@"You haven't done the thing yet."];
    _Counter = 0;
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

- (IBAction)DoThing {
    ++_Counter;
    [_MessageLabel setText:([NSString stringWithFormat:@"You have done the thing %d times.", _Counter])];
}

@end
```

Make sure it works:

![alt text](images/6.png "It works")

### Embed the WatchOS app in the NativeScript app

Things are about to get weird.

At this point you should have two folders in your workspace: "my-app", containing the native script project, and "myapp", containing the watchos project.

They should be completely independent.

Until now.

In xcode, on your watchos project, look for the location of the build artifact `myapp WatchKit App.app`: 

![alt text](images/7.png "You're a human build step")

That's the watchos app, ready to run. Copy it.

Now open the native script ios build folder:

`my-app/platforms/ios/build/emulator`
![alt text](images/8.png "You're a human build step")

Open `myapp.app` as you would any folder (two finger click > Show Package Contents) and create a new folder inside it called `Watch`:

`my-app/platforms/ios/build/emulator/myapp.app`
![alt text](images/9.png "You're a human build step")

Paste `myapp WatchKit App.app` into to new `Watch` folder.

Now run the thing:

```
tns run ios
```

The exact same apps should be there, but now they're together.

Reset the emulators before running the native script app if you don't believe me.

Reset them even if you do:

![alt text](images/10.png "Just do it")

Now run the thing again:

```
tns run ios
```

Start thinking about how you're gonna automate this unless you want to become a human build step.

**Coming up next**: two-way communication between watch and phone.

---

\* It won't work. The organization identifier is an integral part of the binding between the apps, probably, I don't know. I *could* spend a couple of hours looking at it, or you could just do what I tell you.
